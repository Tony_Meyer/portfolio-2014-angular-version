// create the module and name it thmApp
var thmApp = angular.module('thmApp', [
  'ngRoute',
  'ngSanitize',
  'LoadingIndicator'
]);

  // Preloader screen
  // thmApp.config(function($httpProvider) {
  //     var numLoadings = 0;
  //     var loadingScreen = $('<div style="position:fixed;top:0;left:0;right:0;bottom:0;z-index:10000;background-color:#232527;background-color:rgb(35, 37, 39);"><img style="position:absolute;top:50%;left:50%;" alt="" src="data:image/gif;base64,R0lGODlhQgBCAPMAAP///wAAAExMTHp6etzc3KCgoPj4+BwcHMLCwgAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9VBzMu/8VcRTWsVXFYYBsS4knZZYH4d6gYdpyLMErnBAwGFg0pF5lcBBYCMEhR3dAoJqVWWZUMRB4Uk5KEAUAlRMqGOCFhjsGjbFnnWgliLukXX5b8jUUTEkSWBNMc3tffVIEA4xyFAgCdRiTlWxfFl6MH0xkITthfF1fayxxTaeDo5oUbW44qaBpCJ0tBrmvprc5GgKnfqWLb7O9xQQIscUamMJpxC4pBYxezxi6w8ESKU3O1y5eyts/Gqrg4cnKx3jmj+gebevsaQXN8HDJyy3J9OCc+AKycCVQWLZfAwqQK5hPXR17v5oMWMhQEYKLFwmaQTDgl5OKHP8cQjlGQCHIKftOqlzJsqVLPwJiNokZ86UkjDg5emxyIJHNnDhtCh1KtGjFkt9WAgxZoGNMny0RFMC4DyJNASZtips6VZkEp1P9qZQ3VZFROGLPfiiZ1mDKHBApwisZFtWkmNSUIlXITifWtv+kTl0IcUBSlgYEk2tqa9PhZ2/Fyd3UcfIQAwXy+jHQ8R0+zHVHdQZ8A7RmIZwFeN7TWMpS1plJsxmNwnAYqc4Sx8Zhb/WPyqMynwL9eMrpQwlfTOxQco1gx7IvOPLNmEJmSbbrZf3c0VmRNUVeJZe0Gx9H35x9h6+HXjj35dgJfYXK8RTd6B7K1vZO/3qFi2MV0cccemkkhJ8w01lA4ARNHegHUgpCBYBUDgbkHzwRAAAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9VAjMu/8VIRTWcVjFYYBsSxFmeVYm4d6gYa5U/O64oGQwsAwOpN5skipWiEKPQXBAVJq0pYTqnCB8UU5KwJPAVEqK7mCbrLvhyxRZobYlYMD5CYxzvmwUR0lbGxNHcGtWfnoDZYd0EyKLGAgClABHhi8DmCxjj3o1YYB3Em84UxqmACmEQYghJmipVGRqCKE3BgWPa7RBqreMGGfAQnPDxGomymGqnsuAuh4FI7oG0csAuRYGBgTUrQca2ts5BAQIrC8aBwPs5xzg6eEf1lzi8qf06foVvMrtm7fO3g11/+R9SziwoZ54DoPx0CBgQAGIEefRWyehwACKGv/gZeywcV3BFwg+hhzJIV3Bbx0IXGSJARxDmjhz6tzJs4NKkBV7SkJAtOi6nyDh8FRnlChGoVCjSp0aRqY5ljZjplSpNKdRfxQ8Jp3ZE1xTjpkqFuhGteQicFQ1xmWEEGfWXWKfymPK9kO2jxZvLstW1GBLwI54EiaqzxoRvSPVrYWYsq8byFWxqcOs5vFApoKlEEm8L9va0DVHo06F4HQUA6pxrQZoGIBpyy1gEwlVuepagK1xg/BIWpLn1wV6ASfrgpcuj5hkPpVOIbi32lV3V+8U9pVVNck5ByPiyeMjiy+Sh3C9L6VyN9qZJEruq7X45seNe0Jfnfkp+u1F4xEjKx6tF006NPFS3BCv2AZgTwTwF1ZX4QnFSzQSSvLeXOrtEwEAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvVQIzLv/FSEU1nFYhWCAbEsRx1aZ5UG4OGgI9ny+plVuCBiQKoORr1I4DCyDJ7GzEyCYziVlcDhOELRpJ6WiGGJCSVhy7k3aXvGlGgfwbpM1ACabNMtyHGCAEk1xSRRNUmwmV4F7BXhbAot7ApIXCJdbMRYGA44uZGkSIptTMG5vJpUsVQOYAIZiihVtpzhVhAAGCKQ5vaQiQVOfGr+PZiYHyLlJu8mMaI/GodESg7EfKQXIBtrXvp61F2Sg10RgrBwEz7DoLcONH5oa3fBUXKzNc2TW+Fic8OtAQBzAfv8OKgwBbmEOBHiSRIHo0AWBFMuwPdNgpGFFAJr/li3D1KuAu48YRBIgMHAPRZSeDLSESbOmzZs4oVDaKTFnqZVAgUbhSamVzYJIIb70ybSp06eBkOb81rJklCg5k7IkheBq0UhTgSpdKeFqAYNOZa58+Q0qBpluAwWDSRWYyXcoe0Gc+abrRL7XviGAyNLDxSj3bArey+EuWJ+LG3ZF+8YjNW9Ac5m0LEYv4A8GTCaGp5fykNBGPhNZrHpcajOFi8VmM9i0K9G/EJwVI9VM7dYaR7Pp2Fn3L8GcLxREZtJaaMvLXwz2NFvOReG6Mel+sbvvUtKbmQgvECf0v4K2k+kWHnp8eeO+v0f79PhLdz91sts6C5yFfJD3FVIHHnoWkPVRe7+Qt196eSkongXw4fQcCnW41F9F0+ETAQAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9dAjMu/8VISCWcFiFYIBsS4lbJcSUSbg4aMxrfb68nFBSKFg0xhpNgjgMUM9hZye4URCC6MRUGRxI18NSesEOehIqGjCjUK1pU5KMMSBlVd9LXCmI13QWMGspcwADWgApiTtfgRIEBYCHAoYEA2AYWHCHThZ2nCyLgG9kIgehp4ksdlmAKZlCfoYAjSpCrWduCJMuBrxAf1K5vY9xwmTExp8mt4GtoctNzi0FmJMG0csAwBUGs5pZmNtDWAeeGJdZBdrk6SZisZoaA5LuU17n9jpm7feK53Th+FXs3zd//xJOyKbQGAIriOp1a9giErwYCCJGZEexQ8ZzIP8PGPplDRGtjj7OVUJI4CHKeQhfypxJs6bNDyU11rs5IaTPnBpP0oTncwzPo0iTKjXWMmbDjPK8IShikmfIlVeslSwwseZHn1G0sitY0yLINGSVEnC6lFVXigbi5iDJ8WW2tWkXTpWYd9tdvGkjFXlrdy1eDlOLsG34t9hUwgwTyvV2d6Big4efDe6LqylnDt+KfO6cGddmNwRGf5qcxrNp0SHqDmnqzbBqblxJwR7WklTvuYQf7yJL8IXL2rfT5c7KCUEs2gt/G5waauoa57vk/Ur9L1LXb12x6/0OnVxoQC3lcQ1xXC93d2stOK8ur3x0u9YriB+ffBl4+Sc5158LMdvJF1Vpbe1HTgQAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvXQMzLv/lTEUliBYxWCAbEsRwlaZpUC4OCgKK0W/pl5uWCBVCgLE7ERBxFDGYUc0UDYFUclvMkhWnExpB6ERAgwx8/Zsuk3Qh6z4srNybb4wAKYHIHlzHjAqFEh2ABqFWBRoXoESBAVmEkhZBANuGJeHXTKMmDkphC8amUN8pmxPOAaik4ZzSJ4ScIA5VKO0BJOsCGaNtkOtZY9TAgfBUri8xarJYsOpzQAIyMxjVbwG0tN72gVxGGSl3VJOB+GaogXc5ZoD6I7YGpLuU/DI9Trj7fbUyLlaGPDlD0OrfgUTnkGosAUCNymKEGzYIhI+JghE0dNH8QKZY+j/8jEikJFeRwwgD4xAOJChwowuT8qcSbOmzQ5FRugscnNCypD5IkYc0VML0JB9iipdyrQptIc9yRyysC1jETkzU2IxZfVqgYk2yRxNdxUB2KWRUtK65nSX02Lb2NoTETOE1brNwFljse2q25MiQnLUZPWsTBghp76QiLegXpXi2GlrnANqCHCz9g3uVu0AZYMZDU8zEFKuZtHdSKP7/Cb0r7/KDPwCaRr010kkWb8hkEq15xyRDA/czIr3JNWZdcCeYNbUQLlxX/CmCgquWTO5XxzKvnt5ueGprjc5tC0Vb+/TSJ4deNbsyPXG54rXHn4qyeMPa5+Sxp351JZU6SbMGXz+2YWeTOxZ4F4F9/UE4BeKRffWHgJ6EAEAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvXQMzLv/lTEglmYhgwGuLEWYlbBVg0C0OCim9DwZMlVuCECQKoVRzCdBCAqWApTY2d0oqOkENkkeJ04m9fIqCCW7M0BGEQnUbu34YvD2rhIugMDGBucdLzxgSltMWW0CAl9zBAhqEnYTBAV4ZAOWBU8WdZYrWZBWY3w2IYpyK3VSkCiMOU6uboM4dQNmbQSQtI+Jf0Sqt4Acsp45tcHCpr5zqsXJfLOfBbwhzsl7unWbFwhSlddUTqcclN664IE1iq5k3tTow5qn53Td3/AcCAdP9FXv+JwQWANIEFfBZAIjSRHY7yAGSuoESHDkbWFDhy8U7dsnxwBFbw7/O2iUgYxOrpDk7qFcybKly5cIK7qDSUHjgY37uumcNo3mBAE3gQaV6LOo0aNI4XkcGFJnFUc62bEUesCWJYpR/7nMeDPoFCNGTiatBZSogYtHCTBN2sIjWnAi1po08vaavqpy0UBlyFJE15L1wNaF9yKo1ImCjTq5KWYS3xCDh2gFUOcAqg8G6AK8G3lY2M4sgOzL+/QxQANBSQf+dxZ0m5KiD7jObBqx6gsDqlbgMzqHI7E/avu+6Yp3Y8zAHVty20ETo7IWXtz2l1zt1Uz72ty8fM2jVrVq1GK5ieSmaxC/4TgKv/zmcqDHAXmHZH23J6CoOONLPpG/eAoFZIdEHHz4LEWfJwSY55N30RVD3IL87VFMDdOh9B88EQAAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvbQUzLv/lVEg1jBYyGCAbEsRw1aZ5UC4OCiq80kZplVuCECQKprjhEZJyZpPIkZUuL1iPeRAKSEIfFIOQiOUAAtlANMc/Jm4YQsVXuAtwQAYvtiOcwhkTVsZUU5uAlZ+BghpEkkvaB2AiQB1UWZVOWORP3WNOAZflABAApc6m41jcDiGh3agqT8Eny4GtK+1LHO6fmxfvbsanL4hJrBhi5nFFV7IIJOfBsF+uCEIphiAI6PMLikC2VObjN62A+E2H9sj1OYi6cQetxrd5hXYpu5y1vfj9v4CXpgmkBkBK6sQ9CvYYke6LqtGGNknEEa4i+LMHBwxgqEHdOn/ynG4RTHgJI8oU6pcyXKlkZcwW5Y4gPGiEY4JZc6gyVPAgT06gwodStQjSaFjAGokEDOoz3iUmMJUWNKfxZ7iXh6sarTOUzNcZS4sqmgsQxFKRzI1WxDBgZ8Ub0llK7DUW3kD54YtBuOtAFYT9BLFdlfbVjl7W4jslHEX08Qf3AqAPItqwFA00+o4SLcYZkRSblmeMI2yiDSf98ode1hKgZ8hnmq+wLmRXMoE3o7CDPTD0WYHmxwAPAEblwE05ajzdZsCcjzJJ7zGY+AtceaPK+im8Fb4ASQ0KXdoHvhtmu6kt5P22VvR6CXRJ6Cf4POS2wPip3yqr/17hvjSnVKXGnry+VcefkjNV6AF1gmV2ykKOgIaWRT4FFAEACH5BAkKAAAALAAAAABCAEIAAAT/EMhJq720FMy7/5VREJZmIYUBriwlbpUZD2prf289FUM4pLeghIA4jWKwCWFQrCCaQo4BpRsWoBLZBDEgUZa9aIdwreYoPxfPzMOKLdNjBrhLAgxpCpf+xpy3cll2S1giXX0SU1UST4UIXhhkVXtwgSxECIt/Qng0IW03cZkVZJBBXG6dnqGNZgaLNgYEbD+wLKK2iIkDvLm3rbqVtYhxvm9gxhdEs3DJx7BTTJHAwUJgeRdT1NUrZLyHHpiPztWGvKMgsk/kwVzDsczcHVOm8vY47PfdXo0E8fo2iBQQwGuIuCf/AHLwRpAgtjvqGin0wItgmXkJJ1oopbGjx48g/0MCPNhPZIUBAlKqJLjskct6IlE2VBnGpM2bOHN6lJXPHgqYLmQtA+pRJsFHX1r6ywgSzEoBMJbO6jmRiMwwr3SGo6p1Xtadlla88sdVDIKUq/BJLRsFj0o+ftaaXKLSTVKyOc+mtONiaiWA6NRAjXXggF1detmSKnxAsQcDAg4IcHyHMeXHKhUTsKzGsQgzKok+5ozmQM0gA0/fyXxjQOFFmw2LiV0P8gG+ILjAKnz67OEtArDIrCTaBoLCplyfTpnBtIvIv4kV5oucQuEvkmNIvoyhwGvsja0fcFF9AuTB8gwUduNd9fXSfI9PtvdQQmTq45urBqBlovoD9bxn3hd3NsVmgYATRFZcVeiJV4IAC5rEnD0RAAAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9FCHMu/+VgRBWUVhEYYBsS4lbhZyy6t6gaFNFPBmmFW4IIJAqhFEN2bNoiB6YcJL0SUy1IxUL7VSnAGmGJgHuyiZt9wJTA2bg5k++Pa/ZGnBS/dxazW5QBgRgEnsvCIUhShMzVmWMLnuFYoJBISaPOV9IkUOOmJc4gyNgBqddg6YFA3Y3pIl3HWauo5OybCa1Q6SKuCm7s4mKqLgXhBY6moa3xkQpAwPLZVXIzi1A0QWByXvW1xwi2rGbSb7gVNHkLqfn6GHf7/Lh7vM31kZGxfbYM9ED1EaM0MfPi4l/rf6cGsit4JV/PeqpcojhEMWLGDNq3Agln0cjHP8nIBz50WPIhwIGpFRJ5qTLlzBjrkEgLaSGhoYKCDjA80DIaCl7qBnQs+cAnAWhpVwZo6eAbTJ1qARYBCnMeDI7DqgHDohVNkQPtOSHICjXH2EPbL0IRIDbdRjK8hTw9V3blNMApM1LkYDKpxiI1hIxDy6kVq948u1CIOVZEI0PCHjM6y/lcHMvV3bccSfdF8FYiDBlmVfmCoK76Bzrl/MNop8pEOBZl0Pj2GgB31tbYSdVCWX5lh2aEgVUWQh4gkk9wS2P4j/eyjOwc+xONTszOH8++V0ByXrAU+D5Yidp3dcMKK7w/beE7BRYynCruQWX+GIrSGYPncfYedQd4AYZeS+Ix9FsAliwX2+4adTYfwQ+VxtG/V0TAQAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9FCHMu/+VgRCWZhGIAa4sJW6VGRdqa39vPSFFWKS3oIRAqqCKO9gEpdwhhRgDSjccxZoAzRNAKPSgHRGBmqP8XDwybwsOHa9UmcRwpnSBbU55aU3aC090gHlzYyd9c3hRillyEyJUK0SGLlNggpGCWCBSI5GWUF1bmpErUkRkBqUtUmpeq6ZHsIQAgjRtp5S0Ll6MUJ2zuD/BF6ilqrvFxzybhZ7JQl29epO60DheXmwWudbX3Dy9xI+T48kEA8M3qua7rd/wks3x0TUH9wKD9DYiXukSBe4JPCBg3j4+BdINSNekiwCBAg52SJgOUDAEAwxKBCWxo8ePIP9DwhtIUmQFigtTFnhIkqBJMyljfnlJs6bNm/Qwajz4hoNDiDRlMgpIMiPNLjEXwoCoD2e/lEO24VzSbuqHLlUJiVk34N5MiRjztaMjcEDWPHRS+irBUoBUnisXvu1KcOfGhQUxdL0Vwi6YtSL+tSDw0G8QwmYJESZ4loWBAQISg1ksoDEryJIPP6zMy/IjRo8jW6YcaS+YlV9rYW7clbMdgm9BEHYbAnJq2QPYPBxgJy8HjE/icmvaBgFjCrYpCIg4Qfij5bFxPUz98Mny3sx3iIYX0PWQ4xMeulhOJvk1A9VPRq7gEnk+I+S/ebFgWnl2CQjWz/CI/kCk9kvE9xIUAQCGd4AF0NGE3m3XnZSZVfpdEwEAIfkECQoAAAAsAAAAAEIAQgAABP8QyEmrvZQQzLv/laFZCGIRiAGuLCVuFXqmbQ2KNFWGpWr/ANGJ4JvIMghYRgnEvIoSQ7KyQzKD1Sbn6dJAj9Geq3TVhryxnCSLNSHV5gt3Iv0yUUwpXIsYlDV5RB0iX2xRgjUDBwJXc0B6UFgFZR8GB5eRL1p4PAV7K5aXeQaRNaRQep8soQelcWOeri2ssnGptbMCB26vIbGJBwOlYL0hpSKTGIqXBcVNKAXJGAiXi5TOWwjRqhUF1QK42EEE24gfBMu84hfkk+EX2u/OhOv1K8T2Zojf0vmz0NEkFNBVLZg6f3K0RVt4Z+A3hB0WejLHbsBBiF3kYdzIsaPHjyz/CBZcBJKCxJMiCwooOSHagAIvXzZjSbOmzZvitF3kyIkDuWUkS8JkCGVASgF+WEKL+dINwZcaMeoZegjnlqhWO5DDamuKqXQ8B1jUaMDhgQJczUgRO9YDgqfXEJYV28+Ct0U7O/60iMHbJyn5KIbhm0tA3jjohL0yoAtcPQN008YQQFnyKraWgzRGxQ0UnLmKbRCg7JiC0ZlA+qCOgtmG0dJGKMcFgQ52FKo10JWiPCADYQzomMDs7SszlcomBawWm3w15KSPKa8GIJsCZRdIj4cWN9D2aNvX6RhFJfawFsaMtFcI39Lw5O3OAlYwepD9GuUkzGNDf8W+ZvgefWeBEn8AGDUbQuhcRGAfxtnD3DoRAAAh+QQJCgAAACwAAAAAQgBCAAAE/xDISau9lBDMu/8VcRSWZhmEAa4shRxHuVVI2t6gAc+TSaE2nBAwGFgEoxBPApQNPbokpXAQKEMI1a/29FAPWokInFkCwwDgsnuCkSgwREY+QdF7NTTb8joskUY9SxpmBFl7EggDawCAGQd3FyhohoyTOANVen2MLXZ6BghcNwZIZBSZgUOGoJV6KwSmaAYFr54Gs6KHQ6VVnYhMrmxRAraIoaLGpEiRwEx5N5m1J83OTK92v1+Q1ry6vwAIpgLg3dS6yhPbA+nmdqJBHwaZ3OYchtA3BNP2GJf9AD0YCggMlwRTAwqUIygJXwE6BUzBEDCgGsMtoh4+NFOAXpWLHP8y1oh3YZ9FkGlIolzJsqXLlzgkwpgIcwKCAjhzPhSApCcMVTBvCtV4sqbRo0iTshFak1WHfQN6WgmaM5+EiFWqUFxIMJROnDN4UuSX1E5OMVyPGlSKaF+7bqHenogqoKi9fQ/lponIk+zFUAkVthPHc9FLwGA58K17FO9DDBH9PguoMuXjFgSi2u2SWTKvwnpx0MIZ2h/ogLQSlq5QauuW1axJpvac4/QUAW+GKGo2G3ZEwxl4ws5QZE3qzSU9R80NIHO5fUsUMX82/II4drcjFXGR8EdxgPMYoyKHCmhmoM1V9/s9iyIait6x1+mIXEjrNeKmw59SMUSR6l5UE1EjM9txN1049RUUlR771fFfUw1OEJUF38E0TzURJkLbUR31EwEAOwAAAAAAAAAAAA==" /></div>')
  //         .appendTo($('body')).hide();
  //     $httpProvider.responseInterceptors.push(function() {
  //         return function(promise) {
  //             numLoadings++;
  //             loadingScreen.show();
  //             var hide = function(r) { if (!(--numLoadings)) loadingScreen.hide(); return r; };
  //             return promise.then(hide, hide);
  //         };
  //     });
  // });

var module = angular.module('LoadingIndicator', []);
 
module.config(['$httpProvider', function($httpProvider) {
    var interceptor = ['$q', 'LoadingIndicatorHandler', function($q, LoadingIndicatorHandler) {
        return function(promise) {
            LoadingIndicatorHandler.enable();
            
            return promise.then(
                function( response ) {
                    LoadingIndicatorHandler.disable();
                    
                    return response;
                },
                function( response ) {
                    LoadingIndicatorHandler.disable();
                    
                    // Reject the reponse so that angular isn't waiting for a response.
                    return $q.reject( response );
                }
            );
        };
    }];
    
    $httpProvider.responseInterceptors.push(interceptor);
}]);
 
/**
 * LoadingIndicatorHandler object to show a loading animation while we load the next page or wait
 * for a request to finish.
 */
module.factory('LoadingIndicatorHandler', ['$timeout', '$document', function($timeout)
{
    // The element we want to show/hide.
    var $element = $('#loading-indicator');
    
    return {
        // Counters to keep track of how many requests are sent and to know
        // when to hide the loading element.
        enable_count: 0,
        disable_count: 0,
        
        /**
         * Fade the blocker in to block the screen.
         *
         * @return {void}
         */
        enable: function() {
            this.enable_count++;
            
            if ( $element.length ) {
              $element.show(); 
            }
        },
        
        /**
         * Fade the blocker out to unblock the screen.
         *
         * @return {void}
         */
        disable: function() {
            this.disable_count++;
            
            if ( this.enable_count == this.disable_count ) {
                if ( $element.length ) {
                  $(window).scrollTop(0);
                  $timeout(function(){$element.fadeOut();},300); 
                  
                }
            } 
        }
    }
}]);



	// configure routes
	thmApp.config(function($routeProvider) {
		$routeProvider

      // route for the home page
      .when('/', {
        templateUrl : 'pages/home.html',
        controller  : 'mainController'
        //resolve: MyCtrl.resolve
      })

			// route for the portfolio page
			.when('/portfolio', {
				templateUrl : 'pages/portfolio.html',
				controller  : 'portfolioController'
			})

			// route for the skills page
			.when('/skills', {
				templateUrl : 'pages/skills.html',
				controller  : 'skillsController'
			})

			// route for the skills page
			.when('/resume', {
				templateUrl : 'pages/resume.html',
				controller  : 'resumeController'
			})

			// route for the Buzz page
			.when('/buzz', {
				templateUrl : 'pages/buzz.html',
				controller  : 'buzzController'
			})
      // begin portfolio routes - TODO: use UI Router with regex?
      .when('/projects/supporterei/', {templateUrl: 'projects/supporterei/'})
      .when('/projects/basechat/', {templateUrl: 'projects/basechat/'})
      .when('/projects/octopress/', {templateUrl: 'projects/octopress/'})
      .when('/projects/pingzilla/', {templateUrl: 'projects/pingzilla/'})
      .when('/projects/calgary/', {templateUrl: 'projects/calgary/'})
      .when('/projects/design4lifeblog/', {templateUrl: 'projects/design4lifeblog/'})
      .when('/projects/edoc/', {templateUrl: 'projects/edoc/'})
      .when('/projects/hyal/', {templateUrl: 'projects/hyal/'})
      .when('/projects/iamspezial/', {templateUrl: 'projects/iamspezial/'})
      .when('/projects/memantine/', {templateUrl: 'projects/memantine/'})
      .when('/projects/pcan-gateway/', {templateUrl: 'projects/pcan-gateway/'})
      .when('/projects/planet49/', {templateUrl: 'projects/planet49/'})
      .when('/projects/portfolio-2013', {templateUrl: 'projects/portfolio-2013/'})
      .when('/projects/portfolio2009', {templateUrl: 'projects/portfolio2009/'})
      .when('/projects/portfolio2011', {templateUrl: 'projects/portfolio2011/'})
      .when('/projects/portfolio2012', {templateUrl: 'projects/portfolio2012/'})
      .when('/projects/stopandgocoffee', {templateUrl: 'projects/stopandgocoffee/'})
      .when('/projects/valovis', {templateUrl: 'projects/valovis/'})
      .when('/projects/video', {templateUrl: 'projects/video/'})
      .when('/projects/virtualrooms', {templateUrl: 'projects/virtualrooms/'})
      .when('/projects/fileserver', {templateUrl: 'projects/fileserver/'})
      .when('/projects/ng2014', {templateUrl: 'projects/ng2014/'})
      .when('/projects/ancla', {templateUrl: 'projects/ancla/'})
      .when('/projects/pa', {templateUrl: 'projects/pa/'})
      .otherwise({redirectTo: '/'})

	});

//   function MyCtrl($scope, datasets) {    
//     $scope.datasets = datasets;
//   }
  

//   MyCtrl.resolve = {
//     datasets : function($q, $http) {
//         return $http({
//             method: 'GET', 
//             url: 'http://tonymeyer.org/ng2014/img/hero-cat.jpeg'
//         });
//     }
// };
	 // create the controller and inject Angular's $scope
  //thmApp.controller('mainController', ['$scope','$route', '$routeParams', '$location', 
     // active nav stuff
     // function MenuCntl($scope, $route, $routeParams, $location) {
     // $scope.$route = $route;
     // $scope.$location = $location;
     // $scope.$routeParams = $routeParams;
  //}]);

  thmApp.controller('mainController', function($scope) {
            $scope.imageLoaded = function( image ) {
            image.complete = true;
        };

        randOM = function(){
          var min = 1;
          var max = 3;
          // and the formula is:
          var random = Math.floor(Math.random() * (max - min + 1)) + min;
          return random;
        };
        // -------------------------------------- //
        // -------------------------------------- //
        $scope.images = [
             {
                complete: false,
                source: "img/splash/"+randOM()+".jpg"
            }
        ];
  });

	thmApp.controller('portfolioController', function($scope) {
		//$scope.message = 'Look! I am a portfolio page.';
	});

	thmApp.controller('skillsController', function($scope) {
		//$scope.message = 'Look! I am the skills page.';
	});

	thmApp.controller('resumeController', function($scope) {
		//$scope.message = 'Look! I am the timeline page.';
	});

	thmApp.controller('buzzController', function($scope) {
		//$scope.tweetme = tweeter;
	});

//===================================== home-load ===============================
  
  // thmApp.directive("homeLoad",function() { 
  //     return {
  //           link: function (scope, elem, attrs, ctrl) {
  //               var loadHero = function () {                
  //                   $("body").waitForImages({waitForAll: !0,finished: function() {
  //                       $("img").fadeIn()
  //                   }});
  //               }
  //               loadHero();
  //           }
  //         }
  // });

//===================================== active-nav ===============================
  
  thmApp.directive("activeNav",function($location) { 
      return {
            link: function postLink(scope, element, attrs) {
              scope.$on("$routeChangeSuccess", function (event, current, previous) {         
                  // this var grabs the tab-level off the attribute, or defaults to 1
                  var pathLevel = attrs.activeNav || 1,
                  // this var finds what the path is at the level specified
                      pathToCheck = $location.path().split('/')[pathLevel],
                  // this var finds grabs the same level of the href attribute
                      tabLink = attrs.href.split('#')[pathLevel];
                  // now compare the two:
                  if (pathToCheck === tabLink) {
                    element.addClass("active");
                  }
                  else {
                    element.removeClass("active");
                  }
              });
            }
          };
        });

//===================================== port-scripts ===============================
  
  thmApp.directive("portScripts",function() { 
      return {
            link: function (scope, elem, attrs, ctrl) {
              var port = function () {
    
                  $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                      $('body').removeClass('loading');
                    }
                  });
                  if($("[data-repo]")) {
                    $("[data-repo]").github();
                    $(".github-box a").attr("target","_blank");
                  }

                  //FastClick.attach(document.body);
      
        
                  // $('body').ulike({
                  //   icon: 'Heart',
                  //   wording: 'like',
                  //   path: '/ng2014/ulike/',
                  // });

                  setTimeout(function() {
                    $('.thm-likes').remove();
                  }, 100);                      

              }
              port();
              }
            }
    });

//===================================== work-load ===============================
  
  thmApp.directive("workLoad",function() { 
      return {
            link: function (scope, elem, attrs, ctrl) {
                var isoT = function () {                
                          $(function(){
                            $('.post.span4').show();
                            var $container = $('#posts');
                            $container.isotope({
                              itemSelector : '.post'
                            });
                            var $optionSets = $('#page-title .option-set'),
                              $optionLinks = $optionSets.find('a');
                            $optionLinks.click(function(){
                              var $this = $(this);
                              // don't proceed if already selected
                              if ( $this.hasClass('selected') ) {
                                return false;
                              }
                              var $optionSet = $this.parents('.option-set');
                              $optionSet.find('.selected').removeClass('selected');
                              $this.addClass('selected');
                            // make option object dynamically, i.e. { filter: '.my-filter-class' }
                            var options = {},
                              key = $optionSet.attr('data-option-key'),
                              value = $this.attr('data-option-value');
                              
                            // parse 'false' as false boolean
                            value = value === 'false' ? false : value;
                            options[ key ] = value;
                              if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
                              // changes in layout modes need extra logic
                              changeLayoutMode( $this, options )
                            } else {
                              // otherwise, apply new options
                              $container.isotope( options );
                            }    
                            return false;
                            });

                            // next ===================== hover script for dropdowns etc..
                            var options =
                            {
                                dropDownSpeed : 100,
                                slideUpSpeed : 350,
                                slideDownTabSpeed: 50,
                                changeTabSpeed: 200
                            }
                            
                            var methods = 
                            {
                                dropDownMenu : function(e)
                                {  
                                    var body = $(this).find('> :last-child');
                                    var head = $(this).find('> :first-child');
                                    
                                    if (e.type == 'mouseover')
                                    {
                                        body.fadeIn(options.dropDownSpeed);
                                        head
                                    }
                                    else
                                    {
                                        body.fadeOut(options.dropDownSpeed);
                                    }
                                    
                                },
                                
                                dropDownClick : function(e)
                                { 
                                  if (!$(this).hasClass('default-click')){
                                e.preventDefault();
                              }
                                    var dropdown = $(this).parents('.dropdown');
                                    var selected = dropdown.find('.dropmenu .selected');
                                    var newSelect = $(this).html();
                                    var body = dropdown.find('> :last-child');
                                    
                                    dropdown.find('.drop-selected').removeClass('drop-selected');
                                    $(this).addClass('drop-selected');
                                    selected.html(newSelect);
                                    body.fadeOut(options.dropDownSpeed);  
                                    
                                },
                                
                                //param slideFrom - side from where overlay will slide, target - parent element, that contains overlay
                                overlayHover : function(e)
                                {
                                    var overlay;
                                    var slideFrom = e.data.slideFrom;
                                    var content;
                                    
                                    if (typeof e.data.target == 'string')
                                    {   
                                        overlay = $(this).parents(e.data.target).find('.overlay-wrp');
                                        content = $(this).parents(e.data.target).find('.content-wrp');
                                    }
                                    else
                                    {
                                        overlay = $(this).find('.overlay-wrp');
                                        content = $(this).find('.content-wrp');
                                    }
                                    
                                    if ( (e.type == 'mouseover' && !overlay.hasClass('animating')) ||
                                         (e.type == 'click' && !overlay.hasClass('animating') && !overlay.hasClass('active')) )
                                    {
                                        var animation = {};
                                        animation[slideFrom] = '1';
                                        overlay.addClass('animating').addClass('active');
                                        overlay.stop().animate(animation, options.slideUpSpeed, function(){
                                            overlay.removeClass('animating').addClass('active');
                                        });
                                        
                                        animation[slideFrom] = '0.2';
                                        content.parent().css('height', content.parent().height());
                                        content.stop(true,true).animate(animation, options.slideUpSpeed);
                                    }
                                    else if (e.type == 'mouseleave' || (e.type == 'click' && !overlay.hasClass('animating')))
                                    {
                                        var animation = {};
                                        var animationContent = {};
                                        
                                        animationContent[slideFrom] = '1';
                                        content.parent().css('height', content.parent().height());
                                        content.stop(true,true).animate(animationContent, options.slideUpSpeed, function()
                                        {
                                            content.parent().css('height', '');
                                        });
                                        animation[slideFrom] = '0';
                                        overlay.removeClass('active');
                                        overlay.stop(true,true).animate(animation, options.slideUpSpeed);
                                    }
                                }
                            }

                            $('.profile-wrp .profile-photo, .profile-wrp .overlay-wrp').on('click', {slideFrom : 'left', target : '.hover-left'}, methods.overlayHover);
                            $('.hover-down').on('mouseover', {slideFrom : 'opacity'}, methods.overlayHover).on('mouseleave', {slideFrom : 'opacity'}, methods.overlayHover);
                            $('.dropdown').on('mouseover', methods.dropDownMenu).on('mouseleave', methods.dropDownMenu);
                            $('.dropdown .dropmenu-active a').on('click', methods.dropDownClick);


                          }); // end main wrapper fn
                }
                isoT();
            }
          }
  });

//===================================== skills page ===================
 
  thmApp.directive("chartLoad",function() { 
      return {
            link: function (scope, elem, attrs, ctrl) {
                var loadChart = function () {                
                    (function(){
                      var t;
                      function size(animate){
                        if (animate == undefined){
                          animate = false;
                        }
                        clearTimeout(t);
                        t = setTimeout(function(){
                          $("canvas").each(function(i,el){
                            $(el).attr({
                              "width":$(el).parent().width(),
                              "height":$(el).parent().outerHeight()
                            });
                          });
                          redraw(animate);
                          var m = 0;
                          $(".widget").height("");
                          $(".widget").each(function(i,el){ m = Math.max(m,$(el).height()); });
                          $(".widget").height(m);
                        }, 30);
                      }
                      $(window).on('resize', function(){ size(false); });


                      function redraw(animation){
                        var options = {};
                        if (!animation){
                          options.animation = false;
                        } else {
                          options.animation = true;
                        }
                        var data = [
                          {
                            value: 21,
                            color:"#637b85"
                          },
                          {
                            value : 21,
                            color : "#2c9c69"
                          },
                          {
                            value : 21,
                            color : "#dbba34"
                          },
                          {
                            value : 17,
                            color : "#c62f29"
                          },
                          {
                            value : 13,
                            color : "#25A9E7"
                          },
                          {
                            value : 7,
                            color : "#C029C6"
                          }

                        ];
                       
                          var canvas = document.getElementById("hours");
                         if(canvas){
                          var ctx = canvas.getContext("2d");
                          new Chart(ctx).Doughnut(data, options);
                        }


                      }
                      size(true);

                      }());
                }
                loadChart();
            }
          }
  });

//===================================== buzz page ===============================

   
    thmApp.controller("ListController", function( $scope ) {
        // I flag the given images as loaded.
        $scope.imageLoaded = function( image ) {
            image.complete = true;
        };

				randOM = function(){
  				var min = 10;
  				var max = 99;
  				// and the formula is:
  				var random = Math.floor(Math.random() * (max - min + 1)) + min;
  				return random;
				};

				handleTweets = function(tweets){
          var projects = tweets;
          $(".span12").each(function (index) {  
              $(this).find(".tweets").each(function (index) {
                  $(this).html(projects.shift());
              });
          });
				 //$(".user, .interact").remove();
				}
			  twitterFetcher.fetch('405795008655024128', '', 15, true, true, true, '', false, handleTweets, false);
	
				// -------------------------------------- //
        // -------------------------------------- //
        $scope.images = [
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
             {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            },
            {
                complete: false,
                source: "img/lorem/lorem_"+randOM()+".jpeg"
            }
        ];
    });


	 thmApp.directive("thmLoad",function() {
    // I bind the DOM events to the scope.
    function link( $scope, element, attributes ) {
        // I evaluate the expression in the currently
        // executing $digest - as such, there is no need
        // to call $apply().
        function handleLoadSync() {
            //logWithPhase( "handleLoad - Sync" );
            $scope.$eval( attributes.thmLoad );
        }
        // I evaluate the expression and trigger a
        // subsequent $digest in order to let AngularJS
        // know that a change has taken place.
        function handleLoadAsync() {
            //logWithPhase( "handleLoad - Async" );
            $scope.$apply(
                function() {
                    handleLoadSync();
                }
            );

        }
        // I log the given value with the current scope
        // phase.
        function logWithPhase( message ) {
            //console.log( message, ":", $scope.$$phase );
        }

        // Check to see if the image has already loaded.
        // If the image was pulled out of the browser
        // cache; or, it was loaded as a Data URI,
        // then there will be no delay before complete.
        if ( element[ 0 ].src && element[ 0 ].complete ) {
            handleLoadSync();
        // The image will be loaded at some point in the
        // future (ie. asynchronous to link function).
        } else {
            element.on( "load.thmLoad", handleLoadAsync );
        }
        attributes.$observe("src", function( srcAttribute ) {
                //logWithPhase( "$observe : " + srcAttribute );
            }
        );
        $scope.$watch("( image || staticImage ).complete",
            function( newValue ) {
            	$('#render').show();
            	$('.preview-img').fadeTo( "slow", 1 );
            	$('#page-title').spin(false);
            	// count posts
            	var post = $('img.desaturate').length;
					    if(post == '0') {
					    	post = 'error';
					    }
							$('.number').text(post);

              //logWithPhase( "$watch : " + newValue );
            }
        );
        // When the scope is destroyed, clean up.
        $scope.$on("$destroy",function() {
                element.off( "load.thmLoad" );
            }
        );
    }
    // Return the directive configuration.
    return({
    		link: link,
        restrict: "A"
    });

  });

//===================================== global error handler  ===============================

  thmApp.config(['$provide', '$httpProvider', '$compileProvider', function($provide, $httpProvider, $compileProvider) {
        var elementsList = $();

        var showMessage = function(content, cl, time) {
            $('<div/>')
                .addClass('message')
                .addClass(cl)
                .hide()
                .fadeIn('fast')
                .delay(time)
                .fadeOut('fast', function() { $(this).remove(); })
                .appendTo(elementsList)
                .text(content);
        };
        
        $httpProvider.responseInterceptors.push(function($timeout, $q) {
            return function(promise) {
                return promise.then(function(successResponse) {
                    if (successResponse.config.method.toUpperCase() != 'GET')
                        showMessage('Success', 'successMessage', 5000);
                    return successResponse;

                }, function(errorResponse) {
                    switch (errorResponse.status) {
                        case 401:
                            showMessage('Wrong usename or password', 'errorMessage', 20000);
                            break;
                        case 403:
                            showMessage('You don\'t have the right to do this', 'errorMessage', 20000);
                            break;
                        case 500:
                            showMessage('Server internal error: ' + errorResponse.data, 'errorMessage', 20000);
                            break;
                        default:
                            showMessage('Error ' + errorResponse.status + ': ' + errorResponse.data, 'errorMessage', 20000);
                    }
                    return $q.reject(errorResponse);
                });
            };
        });

        $compileProvider.directive('appMessages', function() {
            var directiveDefinitionObject = {
                link: function(scope, element, attrs) { elementsList.push($(element)); }
            };
            return directiveDefinitionObject;
        });
    }]);

//===================================== other code legacy  ===============================
thmApp.directive('globalScripts', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            // FastClick.attach(document.body);
              twitterFetcher.fetch('405795008655024128', 'twitter-feed', 2, true);
              $('.user').remove();
              $('.interact').remove();

              // Mobile Nav
              (function(window, document, undefined)
              {
                  // helper functions
                  var trim = function(str){
                      return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
                  };

                  var hasClass = function(el, cn){
                      return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
                  };

                  var addClass = function(el, cn){
                      if (!hasClass(el, cn)) {
                          el.className = (el.className === '') ? cn : el.className + ' ' + cn;
                      }
                  };

                  var removeClass = function(el, cn){
                      el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
                  };

                  var hasParent = function(el, id){
                      if (el) {
                          do {
                              if (el.id === id) {
                                  return true;
                              }
                              if (el.nodeType === 9) {
                                  break;
                              }
                          }
                          while((el = el.parentNode));
                      }
                      return false;
                  };

                  // normalize vendor prefixes

                  var doc = document.documentElement;

                  var transform_prop = window.Modernizr.prefixed('transform'),
                      transition_prop = window.Modernizr.prefixed('transition'),
                      transition_end = (function() {
                          var props = {
                              'WebkitTransition' : 'webkitTransitionEnd',
                              'MozTransition'    : 'transitionend',
                              'OTransition'      : 'oTransitionEnd otransitionend',
                              'msTransition'     : 'MSTransitionEnd',
                              'transition'       : 'transitionend'
                          };
                          return props.hasOwnProperty(transition_prop) ? props[transition_prop] : false;
                      })();

                  window.App = (function()
                  {

                      var _init = false, app = { };

                      var inner = document.getElementById('inner-wrap'),

                          nav_open = false,

                          nav_class = 'js-nav';


                      app.init = function()
                      {
                          if (_init) {
                              return;
                          }
                          _init = true;

                          var closeNavEnd = function(e)
                          {
                              if (e && e.target === inner) {
                                  document.removeEventListener(transition_end, closeNavEnd, false);
                              }
                              nav_open = false;
                          };

                          app.closeNav =function()
                          {
                              if (nav_open) {
                                  // close navigation after transition or immediately
                                  var duration = (transition_end && transition_prop) ? parseFloat(window.getComputedStyle(inner, '')[transition_prop + 'Duration']) : 0;
                                  if (duration > 0) {
                                      document.addEventListener(transition_end, closeNavEnd, false);
                                  } else {
                                      closeNavEnd(null);
                                  }
                              }
                              removeClass(doc, nav_class);
                          };

                          app.openNav = function()
                          {
                              if (nav_open) {
                                  return;
                              }
                              addClass(doc, nav_class);
                              nav_open = true;
                          };

                          app.toggleNav = function(e)
                          {
                              if (nav_open && hasClass(doc, nav_class)) {
                                  app.closeNav();
                              } else {
                                  app.openNav();
                              }
                              if (e) {
                                  e.preventDefault();
                              }
                          };

                          // open nav with main "nav" button
                          document.getElementById('nav-open-btn').addEventListener('click', app.toggleNav, false);

                          // close nav with main "close" button
                          document.getElementById('nav-close-btn').addEventListener('click', app.toggleNav, false);

                          $('#nav li a').click(function(){
                            app.toggleNav();
                          });

                            var $window = $(window);
                            function checkWidth() {
                                var windowsize = $window.width();
                                if (windowsize > 978) {
                                  app.closeNav();
                                }
                            }
                            // Execute on load
                            checkWidth();
                            // Bind event listener
                            $(window).resize(checkWidth);


                          // close nav by touching the partial off-screen content
                          document.addEventListener('click', function(e)
                          {
                              if (nav_open && !hasParent(e.target, 'nav')) {
                                  e.preventDefault();
                                  app.closeNav();
                              }
                          },
                          true);

                          addClass(doc, 'js-ready');

                      };

                      return app;

                  })();

                  if (window.addEventListener) {
                      window.addEventListener('DOMContentLoaded', window.App.init, false);
                  }

              })(window, window.document);

              // Flikr
              $('#basicuse').jflickrfeed({
                limit: 12,
                qstrings: {
                  id: '33506024@N02'
                },
                itemTemplate: '<li><a rel="photostream" class="fancybox" title="{{title}}" href="{{image_b}}"><i class="icon-search"></i><div class="hover"></div></a><img src="{{image_s}}" alt="{{title}}" /></li>'
              });
              // fancy box
              $(".fancybox").fancybox();
              $('.fancybox-media').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                helpers : {
                  media : {},
                  title : {}
                }
              });
              $(".various").fancybox({
                maxWidth  : 800,
                maxHeight : 600,
                fitToView : false,
                width   : '70%',
                height    : '70%',
                autoSize  : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
              });
              // hero carousel resize fix
              // ====================================================
              window.onresize = function(event){
                  if ($('.hero-carousel').length > 0)
                  {
                      var carousel = $('.hero-carousel'),
                          elements = carousel.children();
                          childWidth = elements.width(),
                      childHeight = elements.height(),
                          carouselWidth = Math.round(childWidth * elements.length),
                          carouselMarginLeft = '-'+ Math.round(childWidth + Math.round(childWidth / 2) ) +'px',
                          carouselPrevMarginLeft = parseFloat(carousel.css('margin-left'));
                          
                      $('.hero-carousel').css({
                      'left': '50%',
                      'width': carouselWidth,
                      'height': childHeight,
                      'margin-left': carouselMarginLeft
                    });
                  }
              }


        } // Link function closing bracket
    };
});



// Directive template
// ====================================================
// App.directive('directiveName', function() {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             $(element).'pluginActivationFunction'(scope.$eval(attrs.directiveName));
//         }
//     };
// });
