
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Stop &#038; Go Coffee</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Stop &#038; Go Coffee<span class="project-mounth">Date: January 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.stopandgocoffee.com/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/stopandgo/sag1.jpg" alt="stopandgocoffee" />
						<img src="img/portfolio/stopandgo/sag2.png" alt="slider image 1" />
					</div>
					<div class="span4 image-desc" ulike-data="stopandgo">
						<h5>Stop and Go Coffee</h5>
            <p>Stop &#038; Go Coffee based in Germany needed a complete branding package. Website design programming and content, logo design, business cards, menu, store window banding and more. I suggested WordPress as a time and cost saver, and they picked a theme framework that they liked.</p>
						<p>Stop &#038; Go Coffee, consistently strives to make and deliver only the absolute best in quality. Everything they do has to be of high quality. They do anything they can to ensure customer satisfaction. Their ongoing mission is to share a passion for great coffee and pastries and being the best coffee and pastries shop in Hesse.
						<a href="http://www.stopandgocoffee.com/" target="_blank">more info..</a></p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/fileserver/" title="File Server"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/portfolio-2013/" title="Portfolio 2013">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
