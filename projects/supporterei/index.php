
  <section class="pTop120"></section>
  <!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
  <div class="background">
        <img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />     
    <div class="pattern"></div>
    <div class="gradient"></div>
  </div>
  <div class="container"><!-- Begin Case -->
  <!-- Start Project Description -->
  
      <div class="container">
    
      <!-- Start Project Description -->
      <div class="row">
        <div class="span8">
          <h1 class="color-text">Supporterei</h1>
        </div>
      </div>
      <div class="row">
        <div class="span8">
          <p class="project-info">Client: Supporterei<span class="project-mounth">Date: May, 2014</span></p>
        </div>
        <div class="span4">
          <a class="launch" href="http://tonymeyer.org/supporterei/" target="_blank"><i class="icon-link"></i><span>Launch</span></a>
        </div>
      </div>
      <div class="row"><div class="span12 line"></div></div>
      <!-- Start Case -->
      <section id="case">
        <div class="row">
          <div class="span8">
            <img class="project-img" src="img/portfolio/supporterei/supporterei_detail.png" alt="supporterei web app" />
          </div>
          <div class="span4 image-desc" ulike-data="calgary">
            <h5>Supporterei</h5>
            <p>This is a JS / PHP app - Design a product and start a campaign for your cause/mission. You can choose to create a t-Shirt, mug or blanket. Add your own text/images to the front and the back, add a description. Then launch the campaign. A back end admin section for user, campaign and account management was developed to round this project off. Finalized campaigns are then sent through the Shopware API to create a campaign page.</p>
          </div>
        </div>
        <div class="row">
          <div class="span8">
            <img class="project-img" src="img/portfolio/supporterei/supporterei_detail_2.png" alt="supporterei web app" />
            <img class="project-img" src="img/portfolio/supporterei/supporterei_detail_3.png" alt="supporterei web app" />
          </div>
          <div class="span4 image-desc">
            <h5>Features</h5>
            <p>
              <ul>
                <li>Responsive design</li>
                <li>Font picker</li>
                <li>Color picker</li>
                <li>Image upload</li> 
                <li>User admin</li>
              </ul>
            </p>
          </div>
        </div>
      </section>
      <!-- End Case -->
    </div>      
  </div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
  <div class="row">
    <div class="span12">
      <a class="arrow pull-left" href="#/projects/pingzilla/" title="pingzilla"><i class="icon-chevron-left"></i>Previous</a>     <a class="arrow pull-right" href="#/projects/edoc/" title="edoc">Next<i class="icon-chevron-right"></i></a>     <a class="back" href="#/portfolio">Back to portfolio</a>
    </div>
  </div>
</section>
<!-- End Pagination -->

  
