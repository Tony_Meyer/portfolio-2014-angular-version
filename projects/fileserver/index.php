
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">File Server</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: PEAK-System Technik GmbH<span class="project-mounth">Date: May, 2012</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.peak-system.com/produktcd/" target="_blank"><i class="icon-link"></i><span>Launch web app</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/filenice/filenice.jpg" alt="file server web app" />
						<img src="img/portfolio/filenice/filenice-open.png" alt="slider image 1" />
						<img src="img/portfolio/filenice/filenice1.png" alt="" />
					</div>
					<div class="span4 image-desc" ulike-data="fileserver">
						<h5>File server web app</h5>
            <p>One of the first tasks I was given at PEAK-System GmbH was to repurpose, refactor, fix, and skin the file serving system they were using. I added a splash of CSS3 and some other design elements, added zip folder download functionality and repaired/customised the existing php/ajax app. </p>
						<h5>Concept &#038; Design</h5>
						<p>Concept and Design was by myself and H.Adamiak @ PEAK-System Technik.</p>
						<h5>Technical Implementation</h5>
						<p>This project is based on an open source app by Andy Beaumont.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/valovis/" title="Valovis Bank"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/stopandgocoffee/" title="Stop & Go Coffee">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	