
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Ancla Logistic</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Ancla Logistic GmBH<span class="project-mounth"></span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.ancla.de/" target="_blank"><i class="icon-link"></i><span>ancla.de</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/ancla/ancla-home.jpg" alt="CalgaryWeddingPix website" />
					</div>
					<div class="span4 image-desc" ulike-data="calgary">
						<h5>Ancla Logistic</h5>
						<p>A simple, clean and responsive website. WordPress was chosen as a CMS here for it&#8217;s user friendly content management system and fast setup.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/pingzilla/" title="pingzilla"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/edoc/" title="edoc">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->

	
