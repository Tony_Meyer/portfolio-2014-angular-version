
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Octopress</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: me<span class="project-mounth">Date: October, 2013</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://thm-design.github.io/tonymeyer/" target="_blank"><i class="icon-link"></i><span>Launch app</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<div class="row project-description">
				<div class="span8">
					<p>My personal website (part of it), converted to an Octopress site. Contemplating switching to a static site generator.</p>
				</div>
			</div>
			<!-- End Project Description -->
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/octopress/octo-home.jpg" alt="basechat website" />
            <img class="project-img" src="img/portfolio/octopress/octo-home2.jpg" alt="basechat website" />
					</div>
					<div class="span4 image-desc" ulike-data="octo">
						<h5>Octopress, Jekyll and co.</h5>
						<p>Octopress is a framework designed by Brandon Mathis for Jekyll, the blog aware static site generator powering Github Pages. To start blogging with Jekyll, you have to write your own HTML templates, CSS, Javascripts and set up your configuration. But with Octopress All of that is already taken care of. Simply clone or fork Octopress, install dependencies and the theme, and you&#8217;re set.</p> 
            <h5>Implementation</h5>
            <p>This is an experimentation and learning project. The development, programming, and deploying of the application was managed by me.</p>
						<h5>Repository</h5>                                                
						<div data-repo="thm-design/tonymeyer" class="github-box-wrap project-single"></div>
            <br/>
				</div>
			</section>
			<!-- End Case -->
		</div>	
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/basechat/" title="Basechat"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/pcan-gateway/" title="PCAN Gateway">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
		