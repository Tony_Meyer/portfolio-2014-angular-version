
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">eDoc solutions</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: eDoc solutions AG<span class="project-mounth">Date: September, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.edoc.de/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>	
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/edoc/edocdetail_1.jpg" alt="eDoc solutions AG website" />
						<img class="project-img" src="img/portfolio/edoc/edocdetail_2.jpg" alt="eDoc solutions AG website" />
					</div>
					<div class="span4 image-desc" ulike-data="edoc">
						<h5>edoc.de</h5>
						<p>I was hired by BLUEMARS in Frankfurt to code up their finished design for eDoc Solutions and create an HTML template and assist in the integration of the site into Typo3 CMS. Technolgies used to create it: HTML, PHP, JavaScript and Typo3.</p>
					</div>
				</div>
				
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/calgary/" title="calgary"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/design4lifeblog/" title="Design4lifeblog">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->

