
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Hyal-System</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Merz Pharmaceuticals<span class="project-mounth">Date: December, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.hyal-system.de/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/merz/hyal-home.jpg" alt="Portfolio 2013" />
				
					</div>
					<div class="span4 image-desc" ulike-data="hyal">
						<h5>Hyal-System</h5>
						<p>While working at GPM mbH I performed various maintenance and udating tasks for the Hyal-System website via FirstSpirit CMS.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/portfolio2012/" title="Portfolio 2012"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/virtualrooms/" title="Virtual Rooms">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	