<section class="pTop120"></section>
  <!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
  <div class="background">
        <img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />     
    <div class="pattern"></div>
    <div class="gradient"></div>
  </div>
  <div class="container"><!-- Begin Case -->
  <!-- Start Project Description -->
  
      <div class="container">
    
      <!-- Start Project Description -->
      <div class="row">
        <div class="span8">
          <h1 class="color-text">ng2014</h1>
        </div>
      </div>
      <div class="row">
        <div class="span8">
          <p class="project-info">Client: me<span class="project-mounth">Date: January, 2014</span></p>
        </div>
        <div class="span4">
          <a class="launch" href="http://tonymeyer.org/ng2014" target="_blank"><i class="icon-link"></i><span>You are here</span></a>
        </div>
      </div>
      <div class="row"><div class="span12 line"></div></div>
      <!-- End Project Description -->
      <!-- Start Case -->
      <section id="case">
        <div class="row">
          <div class="span8">
            <img class="project-img" src="img/portfolio/ng2014/ng2014-home2.jpg" alt="basechat website" />
            <img class="project-img" src="img/portfolio/ng2014/ng2014-home.jpg" alt="basechat website" />
          </div>
          <div class="span4 image-desc" ulike-data="ng2014">
            <h5>ng2014</h5>
            <p>Learning Angular JS!. I converted my current PHP/JS website to an Angular JS project o:</p> 
            <h5>1 UP!</h5>
            <p>I am still learning Angular, but for a weekend crash course/attempt it was much easier than I had anticipated. I learnt about Angular's routing, created some crude directives and a few other things. I use a custom Grunt file to build it all. Next up will be remakeing my Pingzilla app as a SPA with Angular, I will then also integrate Node+Express JS and Socket io.</p>                                               
            <br/>
          </div>
        </div>
      </section>
      <!-- End Case -->
    </div>  
  </div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
  <div class="row">
    <div class="span12">
      <a class="arrow pull-left" href="#/projects/portfolio2009/" title="Portfolio 2009"><i class="icon-chevron-left"></i>Previous</a>      <a class="arrow pull-right" href="#/projects/octopress/" title="Octopress">Next<i class="icon-chevron-right"></i></a>     <a class="back" href="#/portfolio">Back to portfolio</a>
    </div>
  </div>
</section>
