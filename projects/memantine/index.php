
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Mematine</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Merz Phamaceuticals GmbH<span class="project-mounth">Date: August, 2012</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.memantine.com/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- End Project Description -->
			
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/merz/memantine1.jpg" alt="iamspezial website" />
						<img class="project-img" src="img/portfolio/merz/memantine2.jpg" alt="iamspezial website" />
					</div>
					<div class="span4 image-desc" ulike-data="memantine">
						<h5>Mematine.com</h5>
						<p>While working at GPM mbH in Frankfurt I had the opportunity to be a part of the team that created this website. Web technolgies used to create it: HTML, PHP and JavaScript.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/planet49/" title="iPhone 5 Promo"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/portfolio2009/" title="Portfolio 2009">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	