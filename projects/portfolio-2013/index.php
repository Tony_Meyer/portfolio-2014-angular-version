
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Portfolio 2013</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: me<span class="project-mounth">Date: September, 2013</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="../../" target="_blank"><i class="icon-link"></i><span>You are here</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<div class="row project-description">
				<div class="span8">
					<p>This is my latest personal portfolio website, I generally like to create something new every 1-2 years. Keeps me updated.</p>
				</div>
			</div>
			<!-- End Project Description -->
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/portfolio2013/port2013-work.jpg" alt="portfolio 2013 website" />
						<img class="project-img" src="img/portfolio/portfolio2013/port20131.jpg" alt="portfolio 2013 website" />
						<img class="project-img" src="img/portfolio/portfolio2013/port20132.jpg" alt="portfolio 2013 website" />
						<img class="project-img" src="img/portfolio/portfolio2013/port20133.jpg" alt="portfolio 2013 website" />
					</div>
					<div class="span4 image-desc" ulike-data="p2013">
						<h5>Portfolio 2013</h5>
						<p>My last portfolio site was a WordPress powered deal, but this time I wanted to learn how to create my own theme and completely customize it myself. I completely converted a static html site to a fully functional WordPress theme with custom post types and functioning blog and comment system and added my own flavour. So my work here was mainly PHP and WordPress API related. 
							<br/><br/> <b>Edit:</b> I decided to migrate this site to a plain PHP/HTML deal because it was becoming a hassle to deal with WordPress all the time, for this website it was overkill.</p>
					</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/stopandgocoffee/" title="Stop &amp; Go Coffee"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/portfolio2011/" title="Portfolio 2011">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
