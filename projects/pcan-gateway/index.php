
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">PCAN Gateway</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: PEAK-System GmbH<span class="project-mounth">Date: June, 2013</span></p>
				</div>
				<div class="span4">
					<a class="launch" target="_blank"><i class="icon-link"></i><span>Hardware needed</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<div class="row project-description">
				<div class="span8">
					<p>A web interface for PEAK-System GmbH's <a href="http://www.peak-system.com/PCAN-Wireless-Gateway-DR.332.0.html?&L=1" target="_blank"> PCAN-Wireless (and Ethernet) Gateways</a>. Made with PHP and JavaScript. Fully ajaxified and uses the HTML5 History api to keep track of urls. It is also responsive.</p>
				</div>
			</div>
			<!-- End Project Description -->
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/gateway/gateway-long.jpg" alt="gateway website" />
					</div>
					<div class="span4 image-desc" ulike-data="gateway">
						<h5>PCAN Gateway</h5>
						<p>The PCAN-Ethernet/Wireless Gateways allow the connection of different CAN busses over IP networks. CAN frames are wrapped in TCP or UDP message packets and then forwarded via the IP net from one device to the other. The PCAN-Ethernet Gateway DR provides one LAN connection and two High-speed CAN interfaces. With its DIN rail casing and the support of the extended temperature range, the module is suitable for use in an industrial environment.</p> 
            <h5>Web interface</h5>
            <p>The configuration of the device is done via a web interface powered by HTML5, CSS3, PHP and JavaScript. Deep browser support (IE 6) was provided. The interface was created to work with the whole series of PCAN-Gateway products. The PCAN-Ethernet Gateway, PCAN-Wireless Gateway and its Din Rail variants. One code base for all 4 products.</p>
           </div>

					<div class="span8">
							<img class="project-img" src="img/portfolio/gateway/gateway-home.jpg" alt="gateway website" />
							<img class="project-img" src="img/portfolio/gateway/gateway-home2.jpg" alt="gateway website" />
					</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/octopress/" title="Octopress"><i class="icon-chevron-left"></i>Previous</a>						<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	