
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Virtual Rooms</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: TMC The Marketing Company<span class="project-mounth">Date: May, 2009</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="../../../virtualrooms/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/virtualrooms/vm-home.jpg" alt="Portfolio 2013" />
					</div>
					<div class="span4 image-desc" ulike-data="virtualrooms">
						<h5>Virtual Rooms</h5>
						<p>One of the last full Flash websites I built. TMC wanted to a showcase platform for their services. Ahh, the Flash days.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/hyal/" title="Hyal-System"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/video/" title="Videos">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
