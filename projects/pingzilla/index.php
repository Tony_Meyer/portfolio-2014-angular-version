
  <section class="pTop120"></section>
  <!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
  <div class="background">
        <img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />     
    <div class="pattern"></div>
    <div class="gradient"></div>
  </div>
  <div class="container"><!-- Begin Case -->
  <!-- Start Project Description -->
  
      <div class="container">
    
      <!-- Start Project Description -->
      <div class="row">
        <div class="span8">
          <h1 class="color-text">Pingzilla</h1>
        </div>
      </div>
      <div class="row">
        <div class="span8">
          <p class="project-info">Client: None<span class="project-mounth">Date: July, 2013</span></p>
        </div>
        <div class="span4">
          <a class="launch" href="../../../pingzilla/" target="_blank"><i class="icon-link"></i><span>Launch web app</span></a>
        </div>
      </div>
      <div class="row"><div class="span12 line"></div></div>
      <div class="row project-description">
        <div class="span8">
          <p>I made this little app as an experiment to explore PHP, Ajax and CRON. I wanted to create a website uptime monitoring app using cURL. I plan to make a version that uses Node JS in the near future.</p>
        </div>
      </div>
      <!-- End Project Description -->
      <!-- Start Case -->
      <section id="case">
        <div class="row">
          <div class="span8">
            <img class="project-img" src="img/portfolio/pingzilla/pingzilla.jpg" alt="Pingzilla" />
            <img class="project-img" src="img/portfolio/pingzilla/pingzilla2.jpg" alt="Pingzilla" />
          </div>
          <div class="span4 image-desc" ulike-data="pingzilla">
            <h5>Pingzilla Uptime Monitor</h5>
            <p>Sites can be added by clicking the &#8220;Add site&#8221; button, they are entered into a database. Clicking the &#8220;Pingzilla&#8221; button will execute cURL functions that retrieve the current status codes of the sites in the list. CRON jobs execute every minute checking the response headers of the websites listed. If any site is fround to be not accessable, a log entry is written to a file which is ajaxed into the main page with a date stamp and an email notification is sent out.</p>
          </div>
        </div>
      </section>
      <!-- End Case -->
    </div>      
  </div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
  <div class="row">
    <div class="span12">
      <a class="arrow pull-left" href="#/projects/iamspezial/" title="iamspezial"><i class="icon-chevron-left"></i>Previous</a>     <a class="arrow pull-right" href="#/projects/calgary/" title="calgary">Next<i class="icon-chevron-right"></i></a>     <a class="back" href="#/portfolio">Back to portfolio</a>
    </div>
  </div>
</section>
<!-- End Pagination -->
