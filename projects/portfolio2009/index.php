
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Portfolio 2009</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: me<span class="project-mounth">Date: September, 2009</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="../../../portfolio2009/" target="_blank"><i class="icon-link"></i><span>Launch (Turn Sound Down!)</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/port2009.jpg" alt="2009 website" />
						<img class="project-img" src="img/portfolio/port20093.jpg" alt="2009 website" />
						<img class="project-img" src="img/portfolio/port20092.jpg" alt="2009 website" />
					</div>
					<div class="span4 image-desc" ulike-data="p2009">
						<h5>Portfolio 2009</h5>
						<p>2009 version of my personal website. Full Flash (omg). A time before Steve Jobs announced that iOS will never support Flash. Careful this site has a sound track and is unfortunately quite loud, I apologise for that. Be sure to turn down the sound a bit if you launch it. There is a switch to turn it off :)</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="#/projects/memantine/" title="Memantine"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="#/projects/basechat/" title="Basechat">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
