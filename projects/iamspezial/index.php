
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project" port-scripts>
	<div class="background">
				<img src="pages/images/portfolio-bg.jpg" class="" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">MERZ SPEZIAL</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Merz Phamaceuticals GmbH<span class="project-mounth">Date: September, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.merz-spezial.com/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="img/portfolio/merz/iamspezial-home.jpg" alt="iamspezial website" />
					</div>
					<div class="span4 image-desc" ulike-data="iamspezial">
						<h5>I AM Spezial Campaign</h5>
<p>While working at GPM mbH in Frankfurt I had the opportunity to be a part of the team that created this website. Web technolgies used to create it: HTML, PHP, JavaScript. In a sprint phase it was integrated into FirstSpirit CMS.</p>

					</div>
				</div>
				<div class="row">
					<div class="span8">
						<!-- Start Flex Slider -->
						<div class="flexslider">
							<ul class="slides">
								<li>
									<img src="img/portfolio/merz/iamspezial-slider1.jpg" alt="slider image 1" />
								</li>
								<li>
									<img src="img/portfolio/merz/iamspezial-slider2.jpg" alt="" />
								</li>
								<li>
									<img src="img/portfolio/merz/iamspezial-slider3.jpg" alt="" />
								</li>
							</ul>
						</div>
						<!-- End Flex Slider -->
					</div>
					<div class="span4 image-desc">
						<h5>Features</h5>
						<p>
							<ul>
								<li>Video/photo grid</li>
								<li>Background image gallery</li>
								<li>Multiple page layouts</li>
								<li>Live JavaScript validation</li> 
								<li>CMS integration</li>
							</ul>
						</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
						<a class="arrow pull-right" href="#/projects/pingzilla/" title="pingzilla">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="#/portfolio">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	