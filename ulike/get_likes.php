<?php
//Lets get in all the variables being sent in from the ajax post request

$uid = $_POST['uid'];
$page = $_POST['page'];

$error = false;

//Let's check to see if we have the proper variables coming in from the ajax post request
//If not lets load and array with an error and a message

if(!$uid)
{
	$json = array("error"=>true, "message"=>"Could not find uid");
	$error = true;
}else if(!$page)
{
	$json = array("error"=>true, "message"=>"Could not find page");
	$error = true;
}

//If we do not have an error, lets go ahead and start grabbing the records from the database
if(!$error)
{
	include 'mysql_conf.php';//include your mysql configuration file
	
	//Lets try and fetch the like counts for the specific article and page
	$query = mysql_query("SELECT COUNT(u_id) AS u_count FROM tbllikes WHERE u_page = '".$page."' AND u_uid = '".$uid."' LIMIT 1");
	
	$row = mysql_fetch_assoc( $query );
	
	$count = $row['u_count'];
		
	$json = array("error"=>false, "count"=>((!$count)?0:$count));//compile a json array of the counts
}

//Kill all the processes and output the array in json format
die( json_encode( $json ) );