/* 

TO DO

1) Reduce CSS duplication
   - Ideally just a single build - global.scss turns into /build/global.css
   - Can Autoprefixer output minified?
   - If it can, is it as good as cssmin?
   - Could Sass be used again to minify instead?
   - If it can, is it as good as cssmin?

2) Better JS dependency management
   - Require js?
   - Can it be like the Asset Pipeline where you just do //= require "whatever.js"

3) Is HTML minification worth it?

4) Set up a Jasmine test just to try it.

5) Can this Gruntfile.js be abstracted into smaller parts?
   - https://github.com/cowboy/wesbos/commit/5a2980a7818957cbaeedcd7552af9ce54e05e3fb

*/    

module.exports = function(grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          // cssmin will minify later
          style: 'expanded'
        },
        files: {
          'ncss/base.css': 'ncss/sbase.scss'
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 version']
      },
      multiple_files: {
        expand: true,
        flatten: true,
        src: 'ncss/*.css',
        dest: 'ncss/'
      }
    },

    cssmin: {
      combine: {
        files: {
          'ncss/base.min.css': ['ncss/base.css']
        }
      }
    },

    jshint: {
      beforeconcat: ['js/*.js']
    },

    ngmin: {
      controllers: {
      src: 'js/app.js',
      dest: 'js/ngmin/app.ngmin.js'
      }
    },
    uglify: {
      build: {
        src: ['js/plugins.js','js/ngmin/app.ngmin.js'],
        dest: 'js/min/',
        expand: true,  
        flatten: true,
        ext: '.min.js'
      }
    },

    concat: {
      dist: {
        src: [
          'js/min/plugins.min.js',
          'js/libs/angular.min.js',
          'js/libs/angular-mobile.js',
          'js/libs/angular-route.min.js',
          'js/libs/angular-sanitize.min.js',
          'js/ngmin/app.ngmin.js'
          //'js/closure/closure.js'

        ],
        dest: 'js/production/app.js'
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'img/'
        }]
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      scripts: {
        files: ['js/*.js'],
        tasks: ['concat', 'uglify', 'jshint'],
        options: {
          spawn: false,
        }
      },
      css: {
        files: ['css/*.scss'],
        tasks: ['sass', 'autoprefixer', 'cssmin'],
        options: {
          spawn: false,
        }
      },
      images: {
        files: ['img/**/*.{png,jpg,gif}', 'img/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          spawn: false,
        }
      }
    },

    connect: {
      server: {
        options: {
          port: 8000,
          base: './'
        }
      }
    },

  });

  require('load-grunt-tasks')(grunt);
 
  // only ugly
  grunt.registerTask('youugly', ['concat']);
  // only js build
  grunt.registerTask('jsmeh', ['ngmin', 'uglify', 'concat']);
  // only css build
  grunt.registerTask('sasseh', ['sass', 'autoprefixer', 'cssmin']);
  // build all
  grunt.registerTask('default', ['ngmin', 'uglify', 'concat', 'sass', 'autoprefixer', 'cssmin', 'imagemin']);

  grunt.registerTask('dev', ['connect', 'watch']);

};